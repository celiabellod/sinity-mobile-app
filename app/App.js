import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';
import SignIn from './src/Components/SignIn'
import SignUp from './src/Components/SignUp'
import Dashboard from './src/Components/Dashboard'
import {LogBox} from 'react-native'

const App = () => {
  const Stack = createStackNavigator();
  LogBox.ignoreAllLogs(true)
  const[isLoaded] = useFonts({
    Montserrat: require('./assets/fonts/Montserrat-Regular.ttf'),
    MontserratBold: require('./assets/fonts/Montserrat-Bold.ttf'),
    Niconne: require('./assets/fonts/Niconne-Regular.ttf'),
  });

  if (!isLoaded) {
    return <AppLoading />
  }

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="home" component={SignIn} options={{
          headerShown: false,
        }} />
        <Stack.Screen name="signUp" component={SignUp} options={{
          headerShown: false,
        }} />
        <Stack.Screen name="dashboard" component={Dashboard} options={{
          headerShown: false,
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App