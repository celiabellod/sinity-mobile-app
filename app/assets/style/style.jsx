import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    
    //GENERAL
    container: {
        flex: 1,
        backgroundColor: '#07133F',
        color: '#fff',
        paddingVertical: 65,
        justifyContent: 'space-between'
    },
    section: {
        alignItems: 'center',
    },
    input: {
        backgroundColor: 'rgba(255,255,255,0.2)',
        paddingVertical: 18,
        width: 300,
        borderRadius: 50,
        padding: 30,
        fontFamily: 'Montserrat',
        marginBottom: 20
    },
    button: {
        paddingVertical: 8,
        paddingHorizontal: 100,
        borderRadius: 50,
        backgroundColor: '#FF007A',
        marginBottom: 15
    },
    link: {
        fontSize: 14,
        color: '#fff',
        marginTop: 20,
    },
    title2: {
        fontSize: 35,
        color: '#fff',
        fontFamily: 'Niconne',
    },
    icon: {
        marginTop: 20,
        width: 50,
        height: 50,
    },
    text: {
        color: '#fff',
        fontSize: 18,
        fontFamily: 'Montserrat',
    },
    title1: {
        color: '#fff',
        fontSize: 100,
        fontStyle: 'italic',
        textAlign: 'center',
        fontFamily: 'Niconne',
        marginTop: 50
    },
    aroundCircle: {
        width: 70,
        height: 70,
        borderRadius: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    listPuce: {
        width: 10,
        height: 10,
        backgroundColor: '#FF007A',
        borderRadius: 70,
    },
    isPrimary: {
        color: '#FF007A'
    },
    textUnderline: {
        textDecorationLine : 'underline'
    },

    //Sign In
    logo: {
        marginTop: 25,
        width: 80,
        height: 80,
    },
    signInInput: {
        marginTop: 50
    },
    signInContainer: {
        paddingTop: 0,
        paddingBottom: 65,
    },

});