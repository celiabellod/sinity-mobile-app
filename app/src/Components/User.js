import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, Pressable, ScrollView, ImageBackground } from 'react-native';
import styles from '../../assets/style/style';
import { fetchAstroSign, fetchPicture } from '../store/api';

const User = ({ user, setSwiped, swiped, currentUser }) => {

  const [userAstroSign, setUserAstroSign] = useState({})
  const [userPicture, setUserPicture] = useState({})
  const [isLoading, setIsLoading] = useState({})
 
  const age = function getAge(date) { 
    var diff = Date.now() - date.getTime();
    var age = new Date(diff); 
    return Math.abs(age.getUTCFullYear() - 1970);
  }

  const getUserAstroSign = async (id) => {
    const astroSign = await fetchAstroSign(id).then(res=>res)
    setUserAstroSign(astroSign)
  }

  const getUserPicture = async (id) => {
    const picture = await fetchPicture(id).then(res=>res)

    if(picture !== undefined) setUserPicture({uri:URL.createObjectURL(picture)})
  }

  useEffect(() => {
    if(currentUser.email == user.email){
      setSwiped(swiped+1)
    }

    getUserAstroSign(user.astro_sign.id)
    getUserPicture(user.id)
    setIsLoading(false)
  }, [user]);

  useEffect(() => {
    setIsLoading(true)
  }, [userAstroSign, userPicture]);

  return (
    isLoading ?
      <View style={[styles.container, stylesDashboard.container]}>
        <ScrollView>
          <ImageBackground source={userPicture} resizeMode="cover" style={stylesDashboard.image}>
            <View style={stylesDashboard.imageOn}>
              <View style={stylesDashboard.flex}>
                <View>
                  <Text style={stylesDashboard.imageDescriptionText}> { user.firstname }, {age(new Date(user.birthdate))} ans</Text>
                  <Text style={stylesDashboard.imageDescriptionText}>{ userAstroSign.name }</Text>
                  <View style={[styles.aroundCircle, stylesDashboard.aroundCircleSign]}>
                    <Image style={stylesDashboard.aroundCircleSignImage} source={require('../../assets/icon/aquarius.png')} />
                  </View>
                </View>
                <View style={stylesDashboard.imageLike}>
                  <Pressable style={[styles.aroundCircle, stylesDashboard.aroundCircleLike]} onPress={() => { setSwiped(swiped+1) }}>
                    <Image style={stylesDashboard.aroundCircleLikeImage} source={require('../../assets/icon/like.png')} />
                  </Pressable>
                  <Pressable style={[styles.aroundCircle, stylesDashboard.aroundCircleLike]} onPress={() => { setSwiped(swiped+1) }}>
                    <Image style={stylesDashboard.aroundCircleLikeImage} source={require('../../assets/icon/close.png')} />
                  </Pressable>
                </View>
              </View>
            </View>
          </ImageBackground>

          <View style={stylesDashboard.scrollDescription}>
            <View style={stylesDashboard.description}>
              <View style={stylesDashboard.flex}>
                <Text style={[
                  styles.text, 
                  styles.isPrimary, 
                  styles.textUnderline, 
                  stylesDashboard.descriptionHead
                ]}>Je suis</Text>

                <Text style={[styles.text, stylesDashboard.descriptionHead]}>Je recherche</Text>
              </View>
              <View>
                <Text style={[styles.text, stylesDashboard.list]}><View style={styles.listPuce}></View>Signe astrologique</Text>
                <Text style={styles.text}>{ userAstroSign.name }</Text>
              </View>
              { user.biography && <View>
                <Text style={[styles.text, stylesDashboard.list]}><View style={styles.listPuce}></View>Biographie</Text>
                <Text style={styles.text}>{ user.biography }</Text>
              </View> }
            </View>
          </View>

        </ScrollView>

        <View style={stylesDashboard.menu}>
          <Pressable>
            <Image style={stylesDashboard.menuImage} source={require('../../assets/icon/search.png')} />
          </Pressable>
          <Pressable >
            <Image style={stylesDashboard.menuImage} source={require('../../assets/icon/heart.png')} />
          </Pressable>
          <Pressable >
            <Image style={stylesDashboard.menuImage} source={require('../../assets/icon/tap.png')} />
          </Pressable>
          <Pressable >
            <Image style={stylesDashboard.menuImage} source={require('../../assets/icon/file.png')} />
          </Pressable>
          <Pressable >
            <Image style={stylesDashboard.menuImage} source={require('../../assets/icon/user.png')} />
          </Pressable>
        </View>
      </View>
      : <Text>...</Text>
  );
}


const stylesDashboard = StyleSheet.create({
  container: {
    paddingVertical: 0,
    justifyContent: 'flex-start',
    backgroundColor: '#07133F'
  },
  scrollDescription: {
    paddingBottom: 180,
    paddingTop: 50
  },
  description: {
    paddingHorizontal: 30
  },
  imageOn: {
    width: '100%',
    padding: 30,
    paddingBottom: 10
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  image: {
    overflow: 'hidden',
    alignItems: 'center',
    height: 600,
    justifyContent: 'flex-end',
    backgroundColor: '#07133F'
  },
  imageDescriptionText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  aroundCircleSign: {
    backgroundColor: '#FF007A',
    marginVertical: 20
  },
  aroundCircleLike: {
    backgroundColor: '#FFF',
    marginBottom: 20,
  },
  aroundCircleSignImage: {
    width: 50,
    height: 50,
  },
  aroundCircleLikeImage: {
    width: 30,
    height: 30
  },
  menu: {
    width: '100%',
    paddingVertical: 20,
    // position: 'absolute',
    position: 'absolute',
    bottom: 0,
    left: 0,
    flex: 1,
    backgroundColor: '#07133F',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  menuImage: {
    width: 30,
    height: 30
  },
  descriptionHead :{
    fontFamily: 'MontserratBold',
    paddingBottom:20
  },
  list : {
    fontFamily: 'MontserratBold',
    marginTop: 2,
  },
});

export default User;