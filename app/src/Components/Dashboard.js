import React, { useEffect, useState } from 'react'
import User from './User'
import MessageNoUser from '../layout/MessageNoUser'
import { fetchUsers } from '../store/api';
import { getToken } from '../store/auth';

const Dashboard = ({ navigation }) => {

  const [data, setData] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const [swiped, setSwiped] = useState(0)
  const [user, setUser] = useState({})
  const [currentUser, setCurrentUser] = useState({})

  let params = {
    skip: 0,
    limit: 10,  
  };
  
  const users = async (params) => {
    const users = await fetchUsers(params).then(res=>res)
    setData(users)
    setUser(data[swiped])
    setIsLoading(true)
  } 

  useEffect(() => {
    getCurrentUser()
    users(params)
  }, []);

  useEffect(() => {
    if(data){
      if (swiped == data.length) {
        setSwiped(0)
        if(swiped == 10){
          params = {
            skip: swiped,
            limit: swiped + 10
          };
          users(params)
        } else {
          setIsLoading(false)
          setUser({})
        }
      } else {
        setUser(data[swiped])
      }
    }
  }, [swiped, data]);

  const getCurrentUser = async () => {
    const token = await getToken().then(res=>res)
    setCurrentUser(token.user_data)
  } 

  
  return (
    isLoading && data && currentUser ?
        <User user={user} currentUser={currentUser} setSwiped={setSwiped} swiped={swiped} />
      : <MessageNoUser/>

  )
}


export default Dashboard;