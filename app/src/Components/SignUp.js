import React, { useState, useEffect } from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker'
import Button2 from '../layout/Button2'
import TextInput2 from '../layout/TextInput2'
import styles from '../../assets/style/style'
import SearchableDropdown from 'react-native-searchable-dropdown'
import { fetchGenders, fetchCities, fetchAstroSigns, fetchPostUser , fetchAstroSignByDate, login } from '../store/api'
import { getIdGender } from '../utility/signUp'
import * as translations from '../translations/fr'

const SignUp = ({ navigation }) => {

    const [step, setStep] = useState(0)
    const [loading, setLoading] = useState(false)
    const [value, setValue] = useState()
    const [genders, setGenders] = useState()
    const [dropDownPlaceholder, setDropDownPlaceholder] = useState()
    const [selectAstroSigns, setSelectAstroSigns] = useState([])
    const [selectCities, setSelectCities] = useState([])
    const [responses, setResponses] = useState({
        "firstname": "",
        "email": "",
        "password": "",
        "birthdate": "",
        "astro_sign_id": 0,
        "city_id": 0,
        "gender_id": 0,
        "search_astro_sign_id": 0,
        "search_gender_id": 0,
        "search_city_id": 1
    })
    const steps = [
        {
            button: {
                'H': 'Je suis un homme',
                'F': 'Je suis une femme'
            },
            fieldName: "gender_id",
            type: 'button'
        },
        {
            button: {
                'H': 'Je recherche un homme',
                'F': 'Je recherche une femme'
            },
            fieldName: "search_gender_id",
            type: 'button'
        },
        {
            text: 'Quel est votre date de naissance ?',
            fieldName: "birthdate",
            type: 'text'
        },
        {
            text: 'Quel signe astrologique cherchez-vous ?',
            placeholder: 'Choisissez un signe',
            fieldName: "search_astro_sign_id",
            type: 'select'
        },
        {
            text: 'Quel est votre prénom?',
            placeholder: 'Ex: Marie, John..',
            fieldName: "firstname",
            type: 'text'
        },
        {
            text: 'Dans quel ville habitez-vous ?',
            placeholder: 'Indiquez votre ville',
            fieldName: 'city_id',
            type: 'select'
        },
        {
            text: 'Votre adresse email?',
            placeholder: 'marie@gmail.com',
            fieldName: 'email',
            type: 'text'
        },
        {
            text: 'Veuillez choisir un mot de passe',
            placeholder: '*******',
            fieldName: 'password',
            type: 'password'
        },
    ]

    const getGenders = async () => {
        const allGenders = await fetchGenders().then(res=>res)
        setGenders(allGenders)
    }

    const getCities = async () => {
        const cities = await fetchCities().then(res=>res)
        if(cities){
            cities.map(city => setSelectCities(prevItems => [...prevItems, {
                id: city.id,
                name: city.name
            }]))
        }
       
    }

    const getAstroSigns = async () => {
        const astroSigns = await fetchAstroSigns().then(res=>res)
        if(astroSigns){
                astroSigns.map((astroSign) =>
                setSelectAstroSigns((prevItems) => [
                ...prevItems,
                {
                    id: astroSign.id,
                    name: translations[astroSign.name],
                },
            ]))
        }
    }

    const getAstroSignByDate = async (date) => {
        const astroSign = await fetchAstroSignByDate(date).then(res=>res)
        
        if(astroSign){
            setResponses({ ...responses, "astro_sign_id": astroSign.id })
        }
    }

    const postUser = async () => {
        const user = await fetchPostUser(responses).then(res=>res)

        if(user){ 
            const token = await login(responses.email, responses.password).then(res=>res)
            if(token){
                navigation.navigate('dashboard')
            }
        }
    }

    useEffect(() => {      
        getGenders()
        getAstroSigns()
        if (step == 4) {
            getCities()
        }

        if (step === steps.length) {
            postUser(responses)
        }
    }, [step]);


    useEffect(() => {
        if (responses.gender_id != 0 && step < steps.length) {
            setLoading(false)
            setStep(step + 1)
        }
    }, [
        responses.firstname,
        responses.email,
        responses.gender_id,
        responses.search_gender_id,
        responses.password,
        responses.city_id,
        responses.search_astro_sign_id,
        responses.birthdate
    ]);

    useEffect(() => {
        if (responses.birthdate != "") {
            getAstroSignByDate(responses.birthdate)
        }
    }, [responses.birthdate]);

    const persistResponse = (response) => {
        setLoading(true)
        if (response != undefined) {
            if (step === 0 || step === 1) {
                var responseRefact = getIdGender(genders, response)
            }

            if (step === 2) {
                let date = response.toISOString().split('T')[0]
                var responseRefact = date
            }

            setResponses({ ...responses, [steps[step].fieldName]: (responseRefact !== undefined) ? responseRefact : response })
        }
    }

    const onChangeDate = (event, selectedDate) => {
        setValue(selectedDate);
    };

    const buttons = []
    if (step != steps.length) {
        if (steps[step].button) {
            Object.entries(steps[step].button).map((button, index) => {
                const label = button[1]

                return buttons.push(
                    <Button2 color="rgba(255,255,255,0.2)" title={label} disabled={loading} handlePress={() => { persistResponse(label) }} key={index} />
                )
            })
        }
    }

    return (
        step < steps.length ?
            <View style={[styles.container, styles.section]}>
                <View style={styles.section}>
                    <Text style={styles.title2}>Sinity</Text>
                    <Image style={styles.icon} source={require('../../assets/logo.png')} />
                </View>

                {buttons && <View>{buttons}</View>}

                {step == 2 &&
                    <View>
                        <Text style={stylesSignUp.text}>{steps[step].text}</Text>
                        <DateTimePicker
                            value={value != undefined ? value : new Date}
                            mode='date'
                            display="spinner"
                            locale="fr-FR"
                            themeVariant="light"
                            textColor="white"
                            onChange={onChangeDate}
                            maximumDate={new Date("2003-12-31")}
                        />
                    </View>
                }

                {steps[step].placeholder &&
                    <View>
                        <Text style={stylesSignUp.text}>{steps[step].text}</Text>
                        {(step == 3 || step == 5) ?
                            <SearchableDropdown
                                placeholder={dropDownPlaceholder}
                                onTextChange={(text) => setValue(text)}
                                onItemSelect={(item) => {setValue(item.id), setDropDownPlaceholder(item.name) }}
                                containerStyle={{ padding: 5 }}
                                textInputStyle={{
                                    padding: 12,
                                    borderRadius: 50,
                                    color: '#FFF',
                                    backgroundColor: 'rgba(255,255,255,0.2)',
                                }}
                                itemStyle={{
                                    padding: 10,
                                    marginTop: 2,
                                    backgroundColor: 'rgba(255,255,255,0.2)',
                                    borderRadius: 50,
                                    color: '#FFF'
                                }}
                                itemTextStyle={{
                                    color: '#FFF',
                                }}
                                itemsContainerStyle={{
                                    maxHeight: '60%',
                                    color: '#FFF'
                                }}
                                items={step == 5 ? selectCities : selectAstroSigns}
                                defaultIndex={2}
                                resetValue={false}
                                underlineColorAndroid="transparent"
                            />
                            : <TextInput2 placeholder={steps[step].placeholder} value={value} margin={80} onChangeText={setValue} secureTextEntry={steps[step].type == 'password' ? true : false} />}
                    </View>
                }

                {step >= 2 && step < steps.length - 1 && 
                    <Button2 color="#FF007A"
                        title='Suivant'
                        handlePress={() => persistResponse(value)}
                    />
                }
                {step < 2 && 
                    <Button2 color="#FF007A"
                        title='Suivant'
                        opacity={0}
                        handlePress={() => persistResponse(value)}
                    />
                }
                {step == steps.length - 1 &&
                    <Button2 color="#FF007A" title='Je crée mon profil' handlePress={() => persistResponse(value)} />
                }

            </View>
            : <Text style={stylesSignUp.text}>Please wait...</Text>
    );
}

export const stylesSignUp = StyleSheet.create({
    text: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'Montserrat',
        textAlign: 'center',
        marginBottom: 50
    }
})


export default SignUp;