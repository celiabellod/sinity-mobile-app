import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image, Pressable } from 'react-native';
import TextInput2 from '../layout/TextInput2';
import styles from '../../assets/style/style'
import Button2 from '../layout/Button2';
import { login } from '../store/api';

const SignIn = ({ navigation }) => {
  
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const log = async () => {
    const token = await login(email, password).then(res=>res)
    if(token.user_data){
      navigation.navigate('dashboard')
    }
  } 

return (
  <View style={[styles.container, styles.signInContainer]}>

    <View style={styles.section}>
      <Text style={styles.title1}>Sinity</Text>
      <Image style={styles.logo}
        source={require('../../assets/logo.png')} />
      <View style={[styles.signInInput]}>
        <TextInput2 placeholder="Email" onChangeText={setEmail} value={email}/>
        <TextInput2 placeholder="Mot de passe" onChangeText={setPassword} value={password} secureTextEntry={true}/>
      </View>


      <Button2 color="#FF007A" title='Connexion' handlePress={() => { log() }} />

      {/* <Button2 color="#FF007A" title='Connexion' handlePress={() => { navigation.navigate('dashboard') }} /> */}

      <Pressable onPress={() => { navigation.navigate('signUp') }}>
        <Text style={styles.link}>Créer un compte</Text>
      </Pressable>
    </View>
  </View>
);
}

export default SignIn;