import { storeToken, getToken } from "./auth";
import {API_ROOT} from '@env'

export const fetchPostUser = async (responses) => {
  return await fetch(`${API_ROOT}` + "users", {
    method: "POST",
    body: JSON.stringify(responses),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) =>
      response.json().then(async (data) => {
        return data;
      })
    )
    .catch((error) => console.error("erreur de fetch :" + error));
};

export const fetchCities = async () => {
  return fetch(`${API_ROOT}` + "cities", {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) =>
      response.json().then((cities) => {
        return cities;
      })
    )
    .catch((error) => console.error("erreur de fetch :" + error));
};

export const fetchGenders = async () => {
  return await fetch(`${API_ROOT}`+ "genders", {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) =>
      response.json().then((genders) => {
        return genders;
      })
    )
    .catch((error) => console.error("erreur de fetch :" + error));
};

export const login = async (email, password) => {
  let formData = new FormData();
  formData.append("grant_type", "");
  formData.append("username", email);
  formData.append("password", password);
  formData.append("scope", "");
  formData.append("client_id", "");
  formData.append("client_secret", "");


  return await fetch(`${API_ROOT}` + "token", {
    method: "POST",
    headers: {
      Accept: "application/json",
    },
    body: formData,
  })
    .then((response) => response.json())
    .then((token) => {
      storeToken(token);
      return token;
    })
    .catch((error) => console.error(error));
};

export const fetchUsers = async (params) => {
  const token = await getToken().then((res) => res);

  let formData = new FormData();
  formData.append("skip", params.skip);
  formData.append("limit", params.limit);
  
  if(token.token_type){
    return await fetch(`${API_ROOT}` + "users", {
      method: "GET",
      withCredentials: true,
      headers: {
        Authorization: token.token_type.charAt(0).toUpperCase() + token.token_type.slice(1) + " " + token.access_token,
      },
      //body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        return data;
      })
      .catch((error) => console.error("erreur de fetch :" + error));

  }
};

export const fetchPicture = async (id) => {
  const token = await getToken().then((res) => res);

  return await fetch(`${API_ROOT}` + "users/" + id + "/pictures", {
    method: "GET",
    withCredentials: true,
    headers: {
      Authorization: token.token_type.charAt(0).toUpperCase() + token.token_type.slice(1) + " " + token.access_token,
    },
  })
  .then(async (response) => {
      if (response.status === 200) {
        return await response.blob().then((blob) => blob)
      }
    })
    .catch((error) => {
      return error;
    });
};

export const fetchAstroSign = async (id) => {
  const token = await getToken().then((res) => res);
  return await fetch(`${API_ROOT}` + "astroSigns/" + id, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        token.token_type.charAt(0).toUpperCase() +
        token.token_type.slice(1) +
        " " +
        token.access_token,
    },
  })
    .then((response) =>
      response.json().then((data) => {
        return data;
      })
    )
    .catch((error) => console.error("erreur de fetch :" + error));
};

export const fetchAstroSignByDate = async (date) => {
  return await fetch( `${API_ROOT}` + "astroSigns/date?" + new URLSearchParams({ date: date }),
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) =>
      response.json().then((astroSign) => {
        return astroSign;
      })
    )
    .catch((error) => console.error("erreur de fetch :" + error));
};

export const fetchAstroSigns = async () => {
  return await fetch(`${API_ROOT}` + "astroSigns", {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) =>
      response.json().then((astroSigns) => {
        return astroSigns;
      })
    )
    .catch((error) => console.error("erreur de fetch :" + error));
};
