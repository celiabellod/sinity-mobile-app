export const getIdGender = (genders, response) => {
    var genderId;

    if(response.search(/homme/)){
        Object.values(genders).find((gender) => {
            if(gender.name === 'Male') {
                genderId = gender.id
            }
        })
    }

    if(response.search(/femme/)){
        Object.values(genders).find((gender) => {
            if(gender.name === 'Female') {
                genderId = gender.id
            }
        })
    }      

    return genderId
}