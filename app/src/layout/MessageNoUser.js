import React from 'react';
import { View, Text } from 'react-native';

const MessageNoUser = () => {

    return (
        <View>
            <Text>Aucunes personnes n'est disponible dans vos recherches</Text>
        </View>
    );
}


export default MessageNoUser;