import { StyleSheet, TextInput } from 'react-native';
import React, { useState,useEffect } from 'react';

const TextInput2 = ({ placeholder, onChangeText, textValue, secureTextEntry, margin=20 }) => {

    const styleInput = (margin) => {
        return {
            backgroundColor: 'rgba(255,255,255,0.2)',
            paddingVertical: 18,
            width: 300,
            borderRadius: 50,
            padding: 30,
            fontFamily: 'Montserrat',
            color: '#fff',
            marginBottom: margin,
        }
    }

    return (
        <TextInput
            style={styleInput(margin)}
            placeholder={placeholder}
            placeholderTextColor="rgba(255,255,255,0.4)"
            onChangeText={onChangeText}
            value={textValue}
            secureTextEntry={secureTextEntry}
        />
    );
}

export default TextInput2;