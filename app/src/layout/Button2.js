import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';

const Button2 = ({ color, title, handlePress, opacity=1}) => {

    const buttonColor = (color, opacity) => {
        return {
            backgroundColor: color,
            paddingVertical: 11,
            width: 300,
            borderRadius: 50,
            marginTop: 20,
            opacity: opacity
        }
    }


    return (
        <TouchableOpacity style={buttonColor(color, opacity)} onPress={
            handlePress
        } >
            <Text style={stylesButton.buttonText}>{title}</Text>
        </TouchableOpacity >

    );
}

const stylesButton = StyleSheet.create({

    buttonText: {
        color: '#fff',
        fontSize: 17,
        fontFamily: 'Montserrat',
        textAlign: 'center',
    },
})

export default Button2;